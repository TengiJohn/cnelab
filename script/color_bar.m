figure('position',[100,100,500,200]);

imagesc([7,0,-7,0])
axis off
colormap jet
colorbar('Fontsize',30,'Ticks',[-6,-3,0,3,6])
set(gcf,'color','w')